Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :category, only: [:index, :create, :destroy, :update]
      resources :income, only: [:index, :create, :destroy, :update]
      resources :expense, only: [:index, :create, :destroy, :update]
    end
  end
  root 'static#index'
  get '*path' => "static#index", via: :all
end
