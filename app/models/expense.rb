class Expense < ApplicationRecord
  belongs_to :category

  def to_date
    date.strftime("%Y-%m-%d")
  end

  def to_month
    date.strftime("%b %Y")
  end

  def to_year
    date.strftime("%Y")
  end

  def self.total
    sum(:amount)
  end
end
