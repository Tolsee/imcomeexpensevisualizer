class Api::V1::CategoryController < Api::V1::BaseController

  def index
    isGraph = params[:ispiechart] === 'true'
    type = params[:type]
    datePerPage = 10
    pagination = 1

    if params[:page]
      pagination = params[:page].to_i
    end

    offset = (pagination - 1) * datePerPage

    categories = type ? 
      Category.where(category_type: type) :
      Category.offset(offset).limit(datePerPage)


    if isGraph
      categories = categories.map do |category|
        total = 0

        if type == 'income' 
          incomes = category.incomes
          total = incomes.total
        elsif type == 'expense'
          expenses = category.expenses
          total = expenses.total
        end

        ret_category = category.attributes
        ret_category['total'] = total

        ret_category
      end
    end

    respond_with categories
  end

  def create
    respond_with :api, :v1, Category.create(category_params)
  end

  def destroy
    respond_with Category.destroy(params[:id])
  end

  def update
    category = Category.find(params["id"])
    category.update_attributes(category_params)
    respond_with category, json: category
  end

  private def category_params
    params.require(:category).permit(:id, :title, :description, :category_type)
  end
end
