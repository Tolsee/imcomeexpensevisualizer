class Api::V1::IncomeController < Api::V1::BaseController
  def index
    isGraph = params[:type] === 'daily' || params[:type] === 'monthly' || params[:type] === 'yearly'
    datePerPage = 10
    pagination = 1

    if params[:page]
      pagination = params[:page].to_i
    end

    offset = (pagination - 1) * datePerPage

    if params[:startDate] && params[:endDate]
      if isGraph
        incomes = Income.where( date: DateTime.parse(params[:startDate])..DateTime.parse(params[:endDate]) ).order(:date)
      else
        incomes = Income.where( date: DateTime.parse(params[:startDate])..DateTime.parse(params[:endDate]) ).order(:date).offset(offset).limit(datePerPage)
      end
    elsif params[:startDate]
      if isGraph
        incomes = Income.where( "date > ?", DateTime.parse(params[:startDate]) ).order(:date)
      else
        incomes = Income.where( "date > ?", DateTime.parse(params[:startDate]) ).order(:date).offset(offset).limit(datePerPage)
      end
    elsif params[:endDate]
      if isGraph
        incomes = Income.where( "date < ?", DateTime.parse(params[:endDate]) ).order(:date)
      else
        incomes = Income.where( "date < ?", DateTime.parse(params[:endDate]) ).order(:date).offset(offset).limit(datePerPage)
      end
    else
      if isGraph
        incomes = Income.order(:date)
      else
        incomes = Income.order(:date).offset(offset).limit(datePerPage)
      end
    end

    case params[:type]
    when "daily"
      incomes = incomes.group_by(&:to_date)
    when "monthly"
      incomes = incomes.group_by(&:to_month)
    when "yearly"
      incomes = incomes.group_by(&:to_year)
    end

    if isGraph
      incomes = incomes.map do |group|
        date = group[0]
        group_incomes = group[1]

        group_incomes = group_incomes.map { |income| income['amount'] }

        group_incomes = group_incomes.reduce { |sum, amount| sum + amount }

        { :date => date, :value => group_incomes }
      end
    else
      incomes = incomes.map do |income|
        ret_income = income.attributes
        ret_income['category'] = income.category
        ret_income
      end
    end

    respond_with incomes
  end

  def create
    category = Category.find(params[:category_id])
    respond_with :api, :v1, category.incomes.create(income_params)
  end

  def destroy
    respond_with Income.destroy(params[:id])
  end

  def update
    income = Income.find(params['id'])
    income.update_attributes(income_params)
    respond_with income, json: income
  end

  private def income_params
    params.require(:income).permit(:title, :amount, :category_id, :date)
  end

  private def query_params
    # {:type}
  end
end
