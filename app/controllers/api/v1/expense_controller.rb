class Api::V1::ExpenseController < Api::V1::BaseController
  def index
    isGraph = params[:type] === 'daily' || params[:type] === 'monthly' || params[:type] === 'yearly'
    datePerPage = 10
    pagination = 1

    if params[:page]
      pagination = params[:page].to_i
    end

    offset = (pagination - 1) * datePerPage

    if params[:startDate] && params[:endDate]
      if isGraph
        expenses = Expense.where( date: DateTime.parse(params[:startDate])..DateTime.parse(params[:endDate]) ).order(:date)
      else
        expenses = Expense.where( date: DateTime.parse(params[:startDate])..DateTime.parse(params[:endDate]) ).order(:date).offset(offset).limit(datePerPage)
      end
    elsif params[:startDate]
      if isGraph
        expenses = Expense.where( "date > ?", DateTime.parse(params[:startDate]) ).order(:date)
      else
        expenses = Expense.where( "date > ?", DateTime.parse(params[:startDate]) ).order(:date).offset(offset).limit(datePerPage)
      end
    elsif params[:endDate]
      if isGraph
        expenses = Expense.where( "date < ?", DateTime.parse(params[:endDate]) ).order(:date)
      else
        expenses = Expense.where( "date < ?", DateTime.parse(params[:endDate]) ).order(:date).offset(offset).limit(datePerPage)
      end
    else
      if isGraph
        expenses = Expense.order(:date)
      else
        expenses = Expense.order(:date).offset(offset).limit(datePerPage)
      end
    end

    case params[:type]
    when "daily"
      expenses = expenses.group_by(&:to_date)
    when "monthly"
      expenses = expenses.group_by(&:to_month)
    when "yearly"
      expenses = expenses.group_by(&:to_year)
    end

    if isGraph
      expenses = expenses.map do |group|
        date = group[0]
        group_expenses = group[1]

        group_expenses = group_expenses.map { |expense| expense['amount'] }

        group_expenses = group_expenses.reduce { |sum, amount| sum + amount }

        { :date => date, :value => group_expenses }
      end
    else
      expenses = expenses.map do |expense|
        ret_expense = expense.attributes
        ret_expense['category'] = expense.category
        ret_expense
      end
    end

    respond_with expenses
  end

  def create
    category = Category.find(params[:category_id])
    respond_with :api, :v1, category.expenses.create(expense_params)
  end

  def destroy
    respond_with Expense.destroy(params[:id])
  end

  def update
    expense = Expense.find(params['id'])
    expense.update_attributes(expense_params)
    respond_with expense, json: expense
  end

  private def expense_params
    params.require(:expense).permit(:id, :title, :amount, :category_id, :date)
  end
end
