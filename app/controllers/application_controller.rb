# Application Controller
class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  private
 
    def record_not_found
      redirect_back(fallback_location: root_path)
    end
end
