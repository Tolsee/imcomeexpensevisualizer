export const getCategory = (type, done) => {
    fetch(`/api/v1/category?type=${type}`,
        {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.head.querySelector('meta[name="csrf-token"]').getAttribute("content")
            }
        })
        .then(res => {
            return res.json();
        })
        .catch(error => {
            done(error);
        })
        .then((res) => {
            console.log(res);
            done(null, res);
        });
};

export const roundToPre = (value, pre) => {
    let retVal = value;
    retVal = Math.round(retVal * Math.pow(10, pre));
    return retVal / Math.pow(10, pre);
};