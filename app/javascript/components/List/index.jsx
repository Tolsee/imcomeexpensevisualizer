import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import moment from 'moment';

import {
    List,
    Button,
    Spin,
    Card,
    Modal,
    message,
    Tag
} from 'antd';

const confirm = Modal.confirm;

import AddCategory from '../Modal/AddCategory';
import AddIncome from '../Modal/AddIncome';
import AddExpense from '../Modal/AddExpense';

const Edit = styled.div`
	&:hover {
		color: #1890ff;
	}
`;

const Delete = styled.div`
	&:hover {
		color: #f5222d;
	}
`;

const StyledCard = styled(Card)`
    padding: 10px;
    margin-top: 24px;
`;

const ListDesc = ({ listType, data }) => {
    if (listType === 'category') {
        return (
            <div>{ data.description }</div>
        )
    } else {
        return (
            <div>{  moment(data.date).format("MMM Do YY") }</div>
        )
    }
};

ListDesc.propTypes = {
    listType: PropTypes.string,
    data: PropTypes.object
};

const StyledContent = styled.div`
    margin-right: 50px;
    display: block;
`;

const SingleContent = styled.div`
    margin-right: 20px;
    display: inline-block;
    padding: 10px 0;
`;

const ListContent = ({ listType, data }) => {
    if (listType === 'category') {
        return (
            <StyledContent>
                <Tag color="#2db7f5">{ data.category_type }</Tag>
            </StyledContent>
        )
    } else {
        return (
            <StyledContent>
                <SingleContent>
                    Category: <Tag color="#2db7f5">{ data.category ? data.category.title : undefined }</Tag>
                </SingleContent>
                <SingleContent>
                    Amount: { data.amount }
                </SingleContent>
            </StyledContent>
        )
    }
};

ListContent.propTypes = {
    listType: PropTypes.string,
    data: PropTypes.object
};

const EditModal = ({ listType, visible, hideEditModal, data, updateParent }) => {
    switch (listType) {
        case 'category':
            return (
                <AddCategory visible={visible} hideModal={hideEditModal} category={data} updateParent={updateParent} />
            );
            break;
        case 'income':
            return (
                <AddIncome visible={visible} hideModal={hideEditModal} income={data} updateParent={updateParent} />
            );
            break;
        case 'expense':
            return (
                <AddExpense visible={visible} hideModal={hideEditModal} expense={data} updateParent={updateParent} />
            );
        default:
            return (<div></div>);
            break;
    }
};

EditModal.propTypes = {
    listType: PropTypes.string,
    visible: PropTypes.bool,
    hideEditModal: PropTypes.func,
    data: PropTypes.object,
    updateParent: PropTypes.func
};

class AppList extends React.Component {
    constructor(props) {
        super(props);

        this.page = 1;

        this.state = {
            loading: true,
            loadingMore: false,
            showLoadingMore: true,
            data: [],
            editModal: false
        };
    }

    componentDidMount() {
        this.updateComponent();
    }

    updateComponent = () => {
        this.page = 1;
        this.setState({ loading: true });
        this.getData((res) => {
            this.setState({
                loading: false,
                data: res,
            });
        });
    };

    getData = (callback) => {
        const url = `/api/v1/${this.props.listType}?page=${this.page}`;

        fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            })
            .then(res => res.json())
            .catch(error => {
                console.log(error);
            })
            .then((res) => {
                if(res && !res.errors) {
                    this.page = this.page + 1;
                    callback(res);
                }
            });
    };

    deleteData = (id, callback) => {
        const url = `/api/v1/${this.props.listType}/${id}`;

        fetch(url, {
                method: 'DELETE',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .catch(error => {
                callback(error);
            })
            .then((res) => {
                callback(null, res);
            });
    };

    onLoadMore = () => {
        this.setState({
            loadingMore: true,
        });
        this.getData((res) => {
            const data = this.state.data.concat(res);
            const showLoadingMore = !(!res.length || (res.length < 10));
            this.setState({
                data,
                loadingMore: false,
                showLoadingMore
            }, () => {
                window.dispatchEvent(new Event('resize'));
            });
        });
    };

    editData = (id) => {
        return () => {
            this.setState({
                editModal: id
            });
        }
    };

    hideEditModal = () => this.setState({editModal: false});

    handleDelete = (id) => {
        const deleteData = this.deleteData;
        const updateComponent = this.updateComponent;

        return () => {
            confirm({
                title: 'Do you want to delete this item?',
                onOk() {
                    deleteData(id, (error) => {
                        if (error) {
                            message.error('Item could not be deleted!')
                        } else {
                            message.success('Item deleted!');
                            updateComponent();
                        }
                    });
                },
                onCancel() {},
            });
        }
    };

    render() {
        const { loading, loadingMore, showLoadingMore, data, editModal } = this.state;

        const loadMore = showLoadingMore ? (
            <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
                {loadingMore && <Spin />}
                {!loadingMore && <Button onClick={this.onLoadMore}>loading more</Button>}
            </div>
        ) : null;

        return (
            <StyledCard>
                <List
                    loading={loading}
                    itemLayout="horizontal"
                    loadMore={loadMore}
                    dataSource={data}
                    renderItem={item => (
                        <List.Item actions={[(
                                <Edit onClick={this.editData(item.id)}>edit</Edit>
                        ), (
                                <Delete onClick={this.handleDelete(item.id)}>delete</Delete>
                        )]}>
                            <List.Item.Meta
                                title={item.title}
                                description={<ListDesc listType={this.props.listType} data={item}/>}
                            />
                            <ListContent
                                data={item}
                                listType={this.props.listType} />
                        </List.Item>
                    )}
                />
                <EditModal
                    visible={!!editModal}
                    data={data.filter(d => (d.id === editModal))[0]}
                    listType={this.props.listType}
                    hideEditModal={this.hideEditModal}
                    updateParent={this.updateComponent}/>
            </StyledCard>
        );
    }
}

AppList.propTypes = {
    listType: PropTypes.string
};

export default AppList;