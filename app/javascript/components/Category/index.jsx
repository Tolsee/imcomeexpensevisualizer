import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import CategoryPieCharts from './CategoryPieCharts';
import List from '../List';

class ExpenseContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <CategoryPieCharts />
                <List listType='category' />
            </div>
        );
    }
}

export default ExpenseContent;