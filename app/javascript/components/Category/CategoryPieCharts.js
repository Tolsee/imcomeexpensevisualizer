import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import moment from 'moment';

import {Row, Col, Card} from 'antd';
import ChartCard from '../Charts/ChartCard';
import PieChart from '../Charts/PieChart';
import AddCategoryModal from '../Modal/AddCategory';

const PieCharts = ({ income, expense, incomeLoading, expenseLoading}) => {
    return (
        <Row gutter={16}>
            <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                <Card title='Income' loading={incomeLoading} bordered><PieChart data={income} /></Card>
            </Col>
            <Col sxs={24} sm={24} md={24} lg={12} xl={12}>
                <Card title='Expense' loading={expenseLoading} bordered><PieChart data={expense} /></Card>
            </Col>
        </Row>
    );
};

PieCharts.propTypes = {
    income:  PropTypes.array,
    expense:  PropTypes.array,
    incomeLoading: PropTypes.bool,
    expenseLoading: PropTypes.bool
};

class CategoryPieCharts extends React.Component {
    constructor() {
        super();

        this.state = {
            modalVisible: false,
            incomeLoading: true,
            expenseLoading: true,
            income: [{title: 'None', total: 0}],
            expense: [{title: 'None', total: 0}],
        }
    }


    componentDidMount() {
        this.updateComponent();
    }

    updateComponent = () => {
        this.setState({
            incomeLoading: true,
            expenseLoading: true

        });

        this.getIncome((err, res) => {
            if (!err) {
                this.setState({
                    income: res,
                    incomeLoading: false
                });
            }
        });

        this.getExpense((err, res) => {
            if (!err) {
                this.setState({
                    expense: res,
                    expenseLoading: false
                });
            }
        });
    };

    getIncome = (callback) => {
        const url = `/api/v1/category?type=income&ispiechart=true`;

        fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }})
            .then(res => res.json())
            .catch(error => {
                callback(error)
            })
            .then((res) => {
                if(res && !res.errors) {
                    this.page = this.page + 1;
                    callback(null, res);
                }
            });
    };

    getExpense = (callback) => {
        const url = `/api/v1/category?type=expense&ispiechart=true`;

        fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }})
            .then(res => res.json())
            .catch(error => {
                callback(error);
            })
            .then((res) => {
                if(res && !res.errors) {
                    this.page = this.page + 1;
                    callback(null, res);
                }
            });
    };

    showModal = (e) => {
        e.preventDefault();

        this.setState({ modalVisible: true });
    };

    hideModal = (e) => {
        e.preventDefault();

        this.setState({ modalVisible: false });
    };

    render() {
        console.log(this.state);
        const { modalVisible, income, expense, incomeLoading, expenseLoading } = this.state;

        return (
            <div>
                <ChartCard
                    name='Category'
                    showModal={this.showModal}
                    Graph={() => (
                        <PieCharts
                            income={income}
                            expense={expense}
                            incomeLoading={incomeLoading}
                            expenseLoading={expenseLoading}/>
                    )}
                    />
                <AddCategoryModal
                    visible={modalVisible}
                    hideModal={this.hideModal}
                    updateParent={this.updateComponent}/>
            </div>
        );
    }
}

export default CategoryPieCharts;