import React from 'react';
import moment from 'moment';

import ChartCard from '../Charts/ChartCard';
import TimeSeriesGraph from '../Charts/TimeSeries';
import AddExpenseModal from '../Modal/AddExpense';

class ExpenseTimeSeries extends React.Component {
    constructor() {
        super();

        this.state = {
            modalVisible: false,
            type: 'daily',
            range: [moment().startOf('month'), moment().endOf('day')],
            loading: true,
            data: []
        }
    }

    componentDidMount() {
        this.updateComponent();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.type !== prevState.type || this.state.range[0] !== prevState.range[0] || this.state.range[1] !== prevState.range[1]) {
            this.updateComponent();
        }
    }

    updateComponent = () => {
        this.setState({
            loading: true
        });

        this.getData((res) => {
            this.setState({
                data: res,
                loading: false
            }, () => {
                window.dispatchEvent(new Event('resize'));
            });
        });
    };

    getData = (callback) => {
        const url = `/api/v1/expense?type=${this.state.type}&startDate=${this.state.range[0].format()}&endDate=${this.state.range[1].format()}`;

        fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }})
            .then(res => res.json())
            .catch(error => {
                console.log(error);
            })
            .then((res) => {
                if(res && !res.errors) {
                    this.page = this.page + 1;
                    callback(res);
                }
            });
    };

    showModal = (e) => {
        e.preventDefault();

        this.setState({ modalVisible: true });
    };

    hideModal = (e) => {
        e.preventDefault();

        this.setState({ modalVisible: false });
    };

    changeType = (type) => {
        return (e) => {
            e.preventDefault();

            this.setState({ type: type });
        };
    };

    changeRange = (date) => {
        this.setState({
            range: [moment(date[0]).startOf('day'), moment(date[1]).endOf('day')]
        });
    };

    render() {
        const { modalVisible, type, data, loading, range } = this.state;

        return (
            <div>
                <ChartCard
                    name='Expense'
                    showModal={this.showModal}
                    changeType={this.changeType}
                    type={type}
                    Graph={TimeSeriesGraph}
                    data={data}
                    loading={loading}
                    changeRange={this.changeRange}
                    range={range}/>
                <AddExpenseModal
                    visible={modalVisible}
                    hideModal={this.hideModal}
                    updateParent={this.updateComponent}/>
            </div>
        );
    }
}

export default ExpenseTimeSeries;