import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import ExpenseTimeSeries from './ExpenseTimeSeries';
import List from '../List';

class ExpenseContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <ExpenseTimeSeries />
                <List listType='expense'/>
            </div>
        );
    }
}

export default ExpenseContent;