import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import moment from 'moment';
import ChartTitle from './ChartTitle';
import ChartActions from './ChartActions';

import { Card } from 'antd';

const StyledCard = styled(Card)`
    padding: 10px;
`;

const ChartCard = ({ name, showModal, changeType, type, Graph, data, loading, range, changeRange }) => {

    return (
        <StyledCard loading={loading} title={
            <ChartTitle buttonText={`Add ${name}`} type={type} range={range} changeRange={changeRange} showModal={showModal} />
        } extra={
            name !== 'Category' ? <ChartActions changeType={changeType} type={type} /> : null
        } >
            <Graph data={data}/>
        </StyledCard>
    );
};

ChartCard.propTypes = {
    name: PropTypes.string,
    showModal: PropTypes.func,
    changeType: PropTypes.func,
    type: PropTypes.string,
    Graph: PropTypes.func,
    data: PropTypes.array,
    loading: PropTypes.bool,
    range: PropTypes.array,
    changeRange: PropTypes.func
};

export default ChartCard;