import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import moment from 'moment';

import { Button, DatePicker } from 'antd';

const { RangePicker } = DatePicker;

const format = {
    daily: 'YYYY/MM/DD',
    monthly: 'YYYY/MM',
    yearly: 'YYYY'
};

const TitleButton = styled(Button)`
    margin-right: 5px;
`;

const TitleWrapper = styled.div`
    max-width: 30%;
    @media (max-width: 600px) {
        max-width: 50%;
    }
`;

const ChartTitle = ({ buttonText, type, range, showModal, changeRange}) => {
    const rangeAccordingInterval = (date) => {
        let retDate = [...date];
        switch (type) {
            case 'daily':
                retDate = [retDate[0].startOf('day'), retDate[1].endOf('day')];
                break;
            case 'monthly':
                retDate = [retDate[0].startOf('month'), retDate[1].endOf('month')];
                break;
            case 'yearly':
                retDate = [retDate[0].startOf('year'), retDate[1].endOf('year')];
                break;

        }
        changeRange(retDate);
    };
    return (
        <TitleWrapper>
            <TitleButton type='primary' icon='plus' onClick={showModal}> {buttonText}</TitleButton>
            { range ? <RangePicker defaultValue={range} format={format[type]} onChange={rangeAccordingInterval}/> : null }
        </TitleWrapper>
    );
};

ChartTitle.propTypes = {
    buttonText: PropTypes.string,
    type: PropTypes.string,
    range: PropTypes.array,
    changeRange: PropTypes.func,
    showModal: PropTypes.func
};

export default ChartTitle;