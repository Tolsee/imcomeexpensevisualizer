import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ActionsWrapper = styled.div`
	display: flex;
	flex-direction: columns;
	width: 30%;
	cursor: pointer;
	@media (max-width: 600px) {
		width: 50%;
	}
`;

const SingleAction = styled.div`
	padding: 5px 10px;
	flex: 1;
	color: ${({isActive}) => isActive ? '#1890ff' : 'rgba(0, 0, 0, 0.65)'};
	&:hover {
		color: #1890ff;
	}
	&:not(:last-child) {
    	border-right: 1px solid #e8e8e8;
	}
`;

const actions = [
    'daily',
    'monthly',
    'yearly'
];

const ChartActions = ({ changeType, type }) => {
    return (
        <ActionsWrapper>
            {actions.map(function (action, i) {
                return (
                    <SingleAction key={i} onClick={changeType(action)} isActive={type === action}>{ action[0].toUpperCase() + action.slice(1) }</SingleAction>
                );
            })}
        </ActionsWrapper>
    )
};

ChartActions.propTypes = {
    changeType: PropTypes.func,
    changeRange: PropTypes.func,
    type: PropTypes.string
};

export default ChartActions;