import React from 'react';
import PropTypes from 'prop-types';

import { roundToPre } from '../../../utils';
import { Chart, Geom, Axis, Tooltip, Coord, Label, Legend } from 'bizcharts';
import { DataSet } from '@antv/data-set';
const { DataView } = DataSet;

const PieChart = ({ data }) => {
    const dv = new DataView();

    dv.source(data).transform({
        type: 'percent',
        field: 'total',
        dimension: 'title',
        as: 'percent'
    });

    const cols = {
        percent: {
            formatter: val => {
                val = roundToPre(val*100, 2) + '%';
                return val;
            }
        }
    };
    return (
        <Chart height={500} data={dv} scale={cols} padding={[ 80, 100, 80, 80 ]} forceFit>
            <Coord type='theta' radius={0.75} />
            <Axis name="percent" />
            <Legend position='bottom' />
            <Tooltip
                showTitle={false}
                itemTpl={`<li>
                    <div style="display: inline-flex"><span style="background-color:{color};" class="g2-tooltip-marker"></span></div>
                    <div style="display: inline-flex">{name}<br>Percentage : {value}<br>Total: {total}</div>
                    </li>`}
            />
            <Geom
                type="intervalStack"
                position="percent"
                color='title'
                tooltip={['title*percent*total',(item, percent, total) => {
                    percent = roundToPre(percent* 100 , 2) + '%';
                    return {
                        name: item,
                        value: percent,
                        total: total
                    };
                }]}
                style={{lineWidth: 1,stroke: '#fff'}}
            >
                <Label content='percent' offset={-40} textStyle={{
                    rotate: 0,
                    textAlign: 'center',
                    shadowBlur: 2,
                    shadowColor: 'rgba(0, 0, 0, .45)'
                }} />
            </Geom>
        </Chart>
    );
};

PieChart.propTypes = {
    data: PropTypes.array
};

export default PieChart;