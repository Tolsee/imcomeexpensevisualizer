import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Chart, Axis, Tooltip, Geom } from 'bizcharts';

const cols = {
    'value': {min: 0},
    'date': {
        type: 'time'
    },
};

const StyledChart = styled(Chart)`
    padding: 10px 20px 0 0;
`;

const TimeSeriesGraph = ({data, loading}) => {
    return (
        <StyledChart loading={loading} height={500} scale={cols} data={data} forceFit>
            <Axis name="date" />
            <Axis name="value" />
            <Tooltip crosshairs={{type : "y"}}/>
            <Geom type="line" position="date*value" size={2} />
            <Geom type='point' position="date*value" size={4} shape={'circle'} style={{ stroke: '#fff', lineWidth: 1}} />
        </StyledChart>
    );
};

TimeSeriesGraph.propTypes = {
    data: PropTypes.array,
    loading: PropTypes.bool
};

export default TimeSeriesGraph;