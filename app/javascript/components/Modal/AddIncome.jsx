import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { 
    Modal,
    Form, 
    Input,
    Select,
    DatePicker, 
    message 
} from 'antd';

import { getCategory } from "../../utils/index";

const FormItem = Form.Item;

const { Option } = Select;

const AddIncomeForm = Form.create()((props) => {
        const { visible, onCancel, onCreate, form, categories, income } = props;
        const { getFieldDecorator } = form;

        return (
            <Modal
                visible={visible}
                title={(income ? 'Edit' : 'Add') + ' a Income'}
                okText={income ? 'Edit' : 'Add'}
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem label="Title">
                        {getFieldDecorator('title', {
                            rules: [{ required: true, message: 'Please provide title of Income!' }],
                            initialValue: income ? income.title : undefined
                        })(
                            <Input placeholder="Title" />
                        )}
                    </FormItem>
                    <FormItem label="Amount">
                        {getFieldDecorator('amount', {
                            rules: [{ required: true, message: 'Please provide amount!' }],
                            initialValue: income ? income.amount : undefined
                        })(
                            <Input type='number' placeholder="Income Amount" />
                        )}
                    </FormItem>
                    <FormItem label="Category">
                        {getFieldDecorator('category_id', {
                            rules: [{ required: true, message: 'Please provide category' }],
                            initialValue: income ? income.category_id : undefined
                        })(
                            <Select
                                style={{ width: 200 }}
                                placeholder="Select a category">
                                { categories.map(category => (
                                    <Option value={category.id} key={category.id}>{category.title}</Option>
                                )) }
                            </Select>
                        )}
                    </FormItem>
                    <FormItem label="Income Date">
                        {getFieldDecorator('date', {
                            rules: [{ required: true, message: 'Please provide category' }],
                            initialValue: income ? moment(income.date) : undefined
                        })(
                            <DatePicker/>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );

    }
);

class AddIncome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: []
        };
    }

    componentDidMount() {
        this.updateCategory();
    }

    updateCategory = () => {
        const update = (err, categories) => {
            categories = categories.map(({id, title}) => {
                return {
                    id,
                    title
                };
            });

            this.setState({
                categories: categories
            });
        };

        getCategory('income', update);
    };

    handleOk = (e) => {
        e.preventDefault();
        const isEdit = !!this.props.income;

        const form = this.form;
        const { hideModal, updateParent } = this.props;

        this.form.validateFields((err, values) => {
            if (!err) {
                let url = isEdit ? `/api/v1/income/${this.props.income.id}` : `/api/v1/income`;
                console.log(url);
                fetch(url,
                    {
                        method: isEdit ? 'PUT' : 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            'X-CSRF-Token': document.head.querySelector('meta[name="csrf-token"]').getAttribute("content")
                        },
                        body: JSON.stringify(values)
                    })
                    .then(res => res.json())
                    .catch(error => {
                        message.error('Error occurred! Please try again.');
                    })
                    .then((res) => {
                        if (!res.errors) {
                            message.success(`${ isEdit ? 'Income edited!' : 'Income added!' }`);
                            form.resetFields();
                            isEdit && hideModal();
                            updateParent && updateParent();
                        } else {
                            message.error('Error occured! Please try again.');
                        }
                    });
            }
        });
    };

    saveFormRef = (form) => {
        this.form = form;
    };

    render() {
        const { visible, hideModal, income } = this.props;
        const { categories } = this.state;

        return (
            <AddIncomeForm
                ref={this.saveFormRef}
                visible={visible}
                onCancel={hideModal}
                onCreate={this.handleOk}
                categories={categories}
                income={income}
            />
        );
    }
}

AddIncome.propTypes = {
    visible: PropTypes.bool,
    hideModal: PropTypes.func,
    income: PropTypes.object,
    updateParent: PropTypes.func
};

export default AddIncome;