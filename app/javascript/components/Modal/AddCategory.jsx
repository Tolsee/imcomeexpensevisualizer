import React from 'react';
import PropTypes from 'prop-types';

import {
    Modal,
    Form,
    Input,
    Radio,
    message
} from 'antd';

const RadioGroup = Radio.Group;

const FormItem = Form.Item,
    TextArea = Input.TextArea;

const AddCategoryForm = Form.create()((props) => {
        const { visible, onCancel, onCreate, form, category } = props;
        const { getFieldDecorator } = form;
        const isEdit = !!props.category;

        return (
            <Modal
                visible={visible}
                title={(category ? 'Edit' : 'Add') + ' a Expense'}
                okText={category ? 'Edit' : 'Add'}
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem label="Title">
                        {getFieldDecorator('title', {
                            rules: [{ required: true, message: 'Please provide title of Income!' }],
                            initialValue: category ? category.title : undefined
                        })(
                            <Input placeholder="Title" />
                        )}
                    </FormItem>
                    <FormItem label="Description">
                        {getFieldDecorator('description', {
                            initialValue: category ? category.description : undefined
                        })(
                            <TextArea placeholder="Description" />
                        )}
                    </FormItem>
                    <FormItem label="Category Type">
                        {getFieldDecorator('category_type', {
                            initialValue: category ? category.category_type : 'income'
                        })(
                            <RadioGroup>
                                <Radio value='income'>Income</Radio>
                                <Radio value='expense'>Expense</Radio>
                            </RadioGroup>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );

    }
);
class AddCategory extends React.Component {
    constructor(props) {
        super(props);
    }

    handleOk = (e) => {
        e.preventDefault();
        this.isEdit = !!this.props.category;

        const form = this.form;
        const { hideModal, updateParent } = this.props;

        this.form.validateFields((err, values) => {
            if (!err) {
                let url = isEdit ? `/api/v1/category/${this.props.category.id}` : `/api/v1/category`;

                fetch(url,
                    {
                        method: isEdit ? 'PUT' : 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(values)
                    })
                    .then(res => {
                        return res.json();
                    })
                    .catch(error => {
                        message.error('Error occurred! Please try again.');
                    })
                    .then((res) => {
                        if (res && !res.errors) {
                            message.success(`${ isEdit ? 'Category edited!' : 'Category added!' }`);
                            form.resetFields();
                            isEdit && hideModal();
                            updateParent && updateParent();
                        } else {
                            message.error('Error occurred! Please try again.');
                        }
                    });
            }
        });
    };

    saveFormRef = (form) => {
        this.form = form;
    };

    render() {
        const { visible, hideModal, category } = this.props;

        return (
            <AddCategoryForm
                ref={this.saveFormRef}
                visible={visible}
                onCancel={hideModal}
                onCreate={this.handleOk}
                category={category}
            />
        );
    }
}

AddCategory.propTypes = {
    visible: PropTypes.bool,
    hideModal: PropTypes.func,
    category: PropTypes.object,
    updateParent: PropTypes.func
};

export default AddCategory;