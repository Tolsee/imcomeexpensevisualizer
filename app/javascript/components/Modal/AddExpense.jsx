import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import {
    Modal,
    Form,
    Input,
    Select,
    DatePicker,
    message
} from 'antd';

import { getCategory } from "../../utils/index";

const FormItem = Form.Item;

const { Option } = Select;

const AddExpenseForm = Form.create()((props) => {
    const { visible, onCancel, onCreate, form, categories, expense } = props;
    const { getFieldDecorator } = form;

    return (
        <Modal
            visible={visible}
            title={(expense ? 'Edit' : 'Add') + ' a Expense'}
            okText={expense ? 'Edit' : 'Add'}
            onCancel={onCancel}
            onOk={onCreate}
        >
            <Form layout="vertical">
                <FormItem label="Title">
                    {getFieldDecorator('title', {
                        rules: [{ required: true, message: 'Please provide title of Expense!' }],
                        initialValue: expense ? expense.title : undefined
                    })(
                        <Input placeholder="Title" />
                    )}
                </FormItem>
                <FormItem label="Amount">
                    {getFieldDecorator('amount', {
                        initialValue: expense ? expense.amount : undefined
                    })(
                        <Input type='number' placeholder="Expense Amount" />
                    )}
                </FormItem>
                <FormItem label="Category">
                    {getFieldDecorator('category_id', {
                        rules: [{ required: true, message: 'Please provide category' }],
                        initialValue: expense ? expense.category_id : undefined
                    })(
                        <Select
                            style={{ width: 200 }}
                            placeholder="Select a category">
                            { categories.map(category => (
                                <Option value={category.id} key={category.id}>{category.title}</Option>
                            )) }
                        </Select>
                    )}
                </FormItem>
                <FormItem label="Expense Date">
                    {getFieldDecorator('date', {
                        rules: [{ required: true, message: 'Please provide expense date!' }],
                        initialValue: expense ? moment(expense.date) : undefined
                    })(
                        <DatePicker />
                    )}
                </FormItem>
            </Form>
        </Modal>
    );

});


class AddExpense extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: []
        };
    }

    componentDidMount() {
        this.updateCategory();
    }

    updateCategory = () => {
        const update = (err, categories) => {
            categories = categories.map(({id, title}) => {
                return {
                    id,
                    title
                };
            });

            this.setState({
                categories: categories
            });
        };

        getCategory('expense', update);
    };

    handleOk = (e) => {
        e.preventDefault();
        const isEdit = !!this.props.expense;

        const form = this.form;
        const { hideModal, updateParent } = this.props;

        this.form.validateFields((err, values) => {
            if (!err) {
                let url = isEdit ? `/api/v1/expense/${this.props.expense.id}` : `/api/v1/expense`;

                fetch(url,
                    {
                        method: isEdit ? 'PUT' : 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            'X-CSRF-Token': document.head.querySelector('meta[name="csrf-token"]').getAttribute("content")
                        },
                        body: JSON.stringify(values)
                    })
                    .then(res => {
                        return res.json();
                    })
                    .catch(error => {
                        message.error('Error occurred! Please try again.');
                    })
                    .then((res) => {
                        if (!res.errors) {
                            message.success(`${ isEdit ? 'Expense edited!' : 'Expense added!' }`);
                            form.resetFields();
                            isEdit && hideModal();
                            updateParent && updateParent();
                        } else {
                            message.error('Error occured! Please try again.');
                        }
                    });
            }
        });
    };

    saveFormRef = (form) => {
        this.form = form;
    };

    render() {
        const { visible, hideModal, expense } = this.props;
        const { categories } = this.state;

        return (
            <AddExpenseForm
                ref={this.saveFormRef}
                visible={visible}
                onCancel={hideModal}
                onCreate={this.handleOk}
                categories={categories}
                expense={expense}
            />
        );
    }
}

AddExpense.propTypes = {
    visible: PropTypes.bool,
    hideModal: PropTypes.func,
    expense: PropTypes.object,
    updateParent: PropTypes.func
};

export default AddExpense;