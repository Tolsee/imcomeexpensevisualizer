import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import IncomeTimeSeries from './IncomeTimeSeries';
import List from '../List';

class IncomeContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <IncomeTimeSeries />
                <List listType='income' />
            </div>
        );
    }
}

export default IncomeContent;