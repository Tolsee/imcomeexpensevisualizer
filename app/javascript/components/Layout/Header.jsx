import React from 'react';
import styled from 'styled-components';
import { Layout, Icon } from 'antd';
import PropTypes from 'prop-types';

const { Header } = Layout;

const StyledHeader = styled(Header)`
    box-shadow: 0px 1px 3px #e8e8e8;
    background: #fff;
    padding: 0;
`;

const CollapseTrigger = styled(Icon)`
    font-size: 18px;
    padding: 0 30px;
    &:hover {
        color: #1890ff;
    };
    transition: all .2s;
`;

const AppHeader = ({ collapsed, trigger }) => {
    return (
        <StyledHeader>
            <CollapseTrigger onClick={trigger} type={collapsed ? 'right' : 'left'} />
        </StyledHeader>
    );
};

AppHeader.propTypes = {
    collapsed: PropTypes.bool,
    trigger: PropTypes.func
};

export default AppHeader;