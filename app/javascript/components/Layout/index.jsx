import React from 'react';
import { Layout } from 'antd';

// Import layout component
import AppSider from './Sider';
import AppHeader from './Header';
import AppContent from './Content';
import AppFooter from './Footer';

export default class MainLayout extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            collapsed: false
        };
    }

    trigger = () => {
        this.setState({ collapsed: !this.state.collapsed });
    };

    render() {
        const { collapsed } = this.state;
        return (
            <Layout>
                <AppSider collapsed={collapsed}/>
                <Layout>
                    <AppHeader collapsed={collapsed} trigger={this.trigger} />
                    <AppContent>main content</AppContent>
                    <AppFooter />
                </Layout>
            </Layout>
        );
    }
}