import React from 'react';
import { Layout } from 'antd';
import styled from 'styled-components';

const FooterText = styled.div`
    font-size: 12px;
    color: rgba(0, 0, 0, 0.65);
`;

const { Footer } = Layout;

const AppFooter = () => {
    return (
        <Footer>
            <FooterText>Income expense visualizer. Made with Rails and React</FooterText>
        </Footer>
    );
}

export default AppFooter;