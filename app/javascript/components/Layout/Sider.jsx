import React from 'react';
import styled from 'styled-components';
import { Layout, Menu, Icon } from 'antd';
import PropTypes from 'prop-types';

import { Link, withRouter } from 'react-router-dom';

const { Sider } = Layout;

const Logo = styled.div`
    font-size: 16px;
    color: #fff;
    padding: 12px 5px;
    display: ${({ collapsed }) => collapsed ? 'none' : 'display'};
`;

const AppSider = (props) => {
    const { collapsed, location } = props;

    const getPosition = () => {
        switch (location.pathname) {
            case '/income':
                return 1;
                break;
            case '/expense':
                return 2;
                break;
            case '/category':
                return 3;
                break;
            default:
                return false;
                break;
        }
    };

    return (
        <Sider
            trigger={null}
            collapsible
            collapsed={collapsed}
        >
            <div className="logo">
                <Logo collapsed={collapsed}>Income/Expense</Logo>
            </div>
            <Menu theme="dark" mode="inline" defaultSelectedKeys={[getPosition() ? `${getPosition()}` : '1']}>
                <Menu.Item key="1">
                    <Link to='/income'>
                        <Icon type="plus" />
                        <span>Income</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="2">
                    <Link to='/expense'>
                        <Icon type="minus" />
                        <span>Expense</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="3">
                    <Link to='/category'>
                        <Icon type="pie-chart" />
                        <span>Category Breakdown</span>
                    </Link>
                </Menu.Item>
            </Menu>
        </Sider>
    )
};

AppSider.propTypes = {
    collapsed: PropTypes.bool
};

export default withRouter(AppSider);
