import React from 'react';
import styled from 'styled-components';

import { Switch, Route, Redirect } from 'react-router-dom';

import { Layout } from 'antd';
import Income from '../Income';
import Expense from '../Expense';
import Category from '../Category';

const { Content } = Layout;

const AppContentWrapper = styled(Content)`
    background: #F8FAFE;
    padding: 0 50px;
`;

const AppContentInner = styled.div`
   padding: 24px; 
   min-height: 450px;
`;

const AppContent = () => {
    return (
        <AppContentWrapper>
            <AppContentInner>
                <Switch>
                    <Route path="/income" component={Income}/>
                    <Route path="/expense" component={Expense}/>
                    <Route path="/category" component={Category}/>
                    <Redirect to='/income' />
                </Switch>
            </AppContentInner>
        </AppContentWrapper>
    );
};

export default AppContent;