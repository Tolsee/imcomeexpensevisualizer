class AddBelongsTo < ActiveRecord::Migration[5.1]
  def change
    add_reference :expenses, :category, foreign_key: true
    add_reference :incomes, :category, foreign_key: true
  end
end
