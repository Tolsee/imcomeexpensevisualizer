class CreateIncomes < ActiveRecord::Migration[5.1]
  def change
    create_table :incomes do |t|
      t.string :title
      t.float :amount

      t.timestamps
    end
  end
end
