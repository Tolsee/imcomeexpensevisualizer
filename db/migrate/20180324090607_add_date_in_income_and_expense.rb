class AddDateInIncomeAndExpense < ActiveRecord::Migration[5.1]
  def change
  	add_column :expenses, :date, :datetime
  	add_column :incomes, :date, :datetime
  end
end
