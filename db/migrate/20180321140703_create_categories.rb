# Category model
# contains
# Title and Description
class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :title
      t.float :description

      t.timestamps
    end
  end
end
