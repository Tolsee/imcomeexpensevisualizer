# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

5.times do
  income_category = Category.create(title: 'Category', description: 'This is a description', category_type: 'income')
  income_category.incomes.create(title: 'Income', amount: rand(50), date: DateTime.now)
  expense_category = Category.create(title: 'Category', description: 'This is a description', category_type: 'expense')
  expense_category.expenses.create(title: 'Expense', amount: rand(45), date: DateTime.now)
end